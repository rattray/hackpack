from django import forms
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.utils import simplejson
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib import auth
from google.appengine.api import images 
from google.appengine.ext import db
from django.template.defaultfilters import slugify

from app.models import *

import os, sys, datetime, copy, logging, settings


# Change everything in this! Makes things pretty fast and easy
# so the basic info about the site is ubiquitous. 
class globvars():
  pages = [
      {'name':'Home', 'url':'../../'}, #Make a "the elements" section
      {'name':'About', 'url':'../../about'}, #Make a Contact section here
      {'name':'More Resources', 'url':'../../resources'},
      {'name':'Setup Tutorial', 'url':'../../tutorial'},
      {'name':'Sample Guestbook App', 'url':'../../guestbook'},
      {'name':'Django Tutorial', 'url':'../../django'}
    ]
  proj_name = "DecisionCandy HackPack"
  founders = [
    {'name':'Alex Rattray',
       'email':'rattray@wharton.upenn.edu',
       'url':'',
       'blurb':'I\'m Alex. I like webdev and most things Seattle.',
       'picture':'http://profile.ak.fbcdn.net/hprofile-ak-ash2/273392_515004220_1419119046_n.jpg'},
    {'name':'Greg Terrono',
       'email':'gterrono@seas.upenn.edu',
       'url':'',
       'blurb':'I\'m Greg. I like webdev and most things Boston. (??)',
       'picture':'http://chucknorri.com/wp-content/uploads/2011/03/Chuck-Norris-14.jpg'},
    ]
  proj_description = "A quick and dirty package of Django on Google App Engine running Python 2.7 with HTML5 Boilerplate and Twitter Bootstrap."
  context = {
      'pages': pages,
      'proj_name': proj_name,
      'founders': founders,
      'proj_description': proj_description,
      }
  
def index(request):
  gv = globvars
  context = {
    'thispage':'Home'
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  return render_to_response('index.html', context)

def about(request):
  gv = globvars
  context = {
    'thispage':'About'
      }
  context = dict(context, **gv.context)
  return render_to_response('about.html', context)

def resources(request):
  gv = globvars
  context = {
    'thispage':'More Resources'
      }
  context = dict(context, **gv.context)
  return render_to_response('resources.html', context)

def tutorial(request):
  gv = globvars
  context = {
    'thispage':'Setup Tutorial'
      }
  context = dict(context, **gv.context)
  return render_to_response('tutorial.html', context)

def guestbook(request):
  gv = globvars
  context = {
    'thispage': 'Sample Guestbook App'
    }
  context = dict(context, **gv.context)
  return render_to_response('guestbook.html', context)

def django(request):
  gv = globvars
  context = {
    'thispage': 'Django Tutorial'
    }
  context = dict(context, **gv.context)
  return render_to_response('django.html', context)

#######below this lies shit from dc

def image_handler(request, ID):
  project_name = str(project).replace('%20',' ')
  logging.warning(project_name)
  image = get_object_or_404(Image, id=ID)
  headers = 'Content-Type: image/png'
  return HttpResponse(image.img, headers)

def signup(request):
  if request.method == 'POST':
    form = SignUpForm(request.POST)
    if form.is_valid():
      userprofile = form.save()
      user = auth.authenticate(username=request.POST['email'], 
        password=request.POST['password'])
      if user is not None:
        auth.login(request, user)
      return HttpResponseRedirect('/loggedin/')
  else:
    form = SignUpForm()
  return render_to_response('signup.html', {'form':form,}, 
    context_instance = RequestContext(request))

def _get_error(errors):#import from templates
  errors = errors.__str__().split('</li>')
  for error in errors:
    return striptags(error)

def _validate_upload(request):
  if request.method == 'POST':
    form = ProjectForm(request.POST)
    reply = {'success': True, 'project': request.POST['name'], 
      'form_valid': True,}
    if not form.is_valid() or int(request.POST['picture_num']) < 2:
      reply['form_valid'] = False
      reply['labels'] = dict()
      reply['add_class'] = dict()
      for field in form.visible_fields():
        label = field.label
        reply['add_class'][field.html_name] = False
        if field.errors:
          label = '%s (%s)' % (label, _get_error(field.errors))
          reply['add_class'][field.html_name] = True
        reply['labels'][field.html_name] = label
      if int(request.POST['picture_num']) < 2:
        reply['labels']['images'] = 'Images (at least two required)'
        reply['add_class']['images'] = True
        reply['form_valid'] = False
      else:
        reply['labels']['images'] = 'Images'
        reply['add_class']['images'] = False
    return reply, form

def upload_file(request):
  if request.method == 'POST':
    for field_name in request.FILES:
      file = request.FILES[field_name].read()
      project = request.POST['project']
      new_image = Image()
      new_image.project = get_object_or_404(Project, name=project)
      new_image.img = db.Blob(file)
      new_image.save()
    return HttpResponse("everything went ok", mimetype="text/plain")
  else:
    raise Http404

def upload(request):
  if request.method == 'POST':
    reply, form = _validate_upload(request)
    if reply['form_valid']:
      project = form.save(creator=request.user.client)
    return HttpResponse(simplejson.dumps(reply),
      content_type='application/json')
  else:
    form = ProjectForm()
  context = {
    'form': form,
    'user': request.user,
    'session_key': request.session.session_key,
  }
  return render_to_response('upload.html', context, 
    context_instance = RequestContext(request))

def login(request): 
  if request.method == 'POST':
    form = LogInForm(request.POST)
    if form.is_valid():
      user = form.user_cache
      if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/loggedin/')
      else:
        return render_to_response('login.html', {'form': form, 
          'user': form.user_cache,}, context_instance=RequestContext(request))
  else:
    form = LogInForm() 
  return render_to_response('login.html', {'form': form, 'user': request.user,},context_instance=RequestContext(request))

def loggedin(request):
  username = request.user.client.name
  return render_to_response('loggedin.html',{'username':username, 'user': request.user,})

def logout(request):
  auth.logout(request)
  return HttpResponseRedirect('/')
